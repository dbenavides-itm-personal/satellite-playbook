# Ansible

```bash
yum install ansible
```

# Current Content Views & Activation Keys

| Activation Key          | Environment     | Content View                |
| :---------------------- | :-------------- | :-------------------------- |
| `demoservers-X`         | `DEV `         | ELX                |

# Vars

* satellite_info.url: URL de Satellite o Capsula.
* satellite_info.org: Organización donde se hará el registro.
* repos_dir: Directorio de repositorios YUM.
* activation_key: Llave que se usara para el registro de hosts.
* ssh_user: Usuario SSH con acceso y permisos a hosts a registrar.

## Execution

```bash
ansible-playbook host-registration-playbook.yml -i INVENTORY -l HOST
```